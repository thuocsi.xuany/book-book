package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type BookUpdate struct {
	Title       *string              `json:"title,omitempty" bson:"title,omitempty" validate:"minLen:3|maxLen:30" message:"minLen:title must be at least 3 characters|maxLen:title must be less than 30 characters"`
	Description string               `json:"description,omitempty" bson:"description,omitempty" validate:"minLen:3|maxLen:50" message:"minLen:description must be at least 3 characters|maxLen:description must be less than 50 characters"`
	Year        *int                 `json:"year,omitempty" bson:"year,omitempty" validate:"min:1900|max:2023" message:"min:year must be at least 1900|max:year must be less than 2023"`
	Price       float64              `json:"price,omitempty" bson:"price,omitempty" validate:"min:0" message:"min:price must be at least 0"`
	Cover       string               `json:"image,omitempty" bson:"image,omitempty" validate:"url" message:"url:image must be a valid url"`
	Stock       int                  `json:"stock,omitempty" bson:"stock,omitempty" validate:"min:0|max:999999" message:"max:stock must be less than 999999"`
	CategoryIDs []primitive.ObjectID `json:"category_ids,omitempty" bson:"category_ids,omitempty"`
	AuthorIDs   []primitive.ObjectID `json:"author_ids,omitempty" bson:"author_ids,omitempty"`

	LastUpdate time.Time `json:"last_update,omitempty" bson:"last_update,omitempty"`
}
