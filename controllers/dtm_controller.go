package controllers

import (
	. "book/config"
	. "book/models"
	"context"
	"errors"
	"sync"

	"github.com/dtm-labs/dtmcli"
	"github.com/dtm-labs/dtmcli/dtmimp"
	"github.com/dtm-labs/dtmcli/logger"
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var (
	mongoOnce sync.Once
	mongoc    *mongo.Client
)

// MongoGet get mongo client
func MongoGet() *mongo.Client {
	mongoOnce.Do(func() {
		uri := "mongodb+srv://ydam:ydam@notes-cluster.iwril.mongodb.net/account?retryWrites=true&w=majority"
		ctx := context.Background()
		client, err := mongo.Connect(ctx, options.Client().ApplyURI(uri))
		dtmimp.E2P(err)
		mongoc = client
	})
	return mongoc
}

// MustBarrierFromGin 1
func MustBarrierFromGin(c *gin.Context) *dtmcli.BranchBarrier {
	ti, err := dtmcli.BarrierFromQuery(c.Request.URL.Query())
	logger.FatalIfError(err)
	return ti
}

func BorrowBookTCCTry(c *gin.Context) interface{} {
	var borrowBook BorrowBook
	if err := c.Bind(&borrowBook); err != nil {
		return err
	}
	bb := MustBarrierFromGin(c)
	return bb.MongoCall(MongoGet(), func(sc mongo.SessionContext) error {

		// check book exist
		var book Book
		err := GlobalBookCollection.FindOne(context.TODO(), bson.M{"_id": borrowBook.BookID}).Decode(&book)
		if err != nil {
			return err
		}

		// check book stock
		if book.Stock == 0 {
			return errors.New("book stock is 0")
		}

		return nil
	})

}

func BorrowBookTCCConfirm(c *gin.Context) interface{} {
	var borrowBook BorrowBook
	if err := c.Bind(&borrowBook); err != nil {
		return err
	}

	bb := MustBarrierFromGin(c)
	return bb.MongoCall(MongoGet(), func(sc mongo.SessionContext) error {

		// // update book stock
		// _, err := GlobalBookCollection.UpdateOne(context.TODO(), bson.M{"_id": borrowBook.BookID}, bson.M{"$inc": bson.M{"stock": -1}})

		// if err != nil {
		// 	return err
		// }

		// borrowBook.LastUpdate = time.Now()

		// // insert borrow_book
		// _, err = GlobalBorrowBookCollection.InsertOne(context.TODO(), borrowBook)
		// if err != nil {
		// 	return err
		// }

		// return nil
		return errors.New("errror confirm")

	})
}

func BorrowBookTCCCancel(c *gin.Context) interface{} {
	var borrowBook BorrowBook
	if err := c.Bind(&borrowBook); err != nil {
		return err
	}
	bb := MustBarrierFromGin(c)
	return bb.MongoCall(MongoGet(), func(sc mongo.SessionContext) error {

		// update book stock
		_, err := GlobalBookCollection.UpdateOne(context.TODO(), bson.M{"_id": borrowBook.BookID}, bson.M{"$inc": bson.M{"stock": 1}})
		if err != nil {
			return err
		}

		// delete borrow_book
		_, err = GlobalBorrowBookCollection.DeleteOne(context.TODO(), bson.M{"user_id": borrowBook.UserID, "book_id": borrowBook.BookID})
		if err != nil {
			return err
		}

		return nil
	})
}
