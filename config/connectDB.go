package config

import (
	"context"
	"fmt"
	"log"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var (
	GlobalClient               *mongo.Client
	GlobalAuthorCollection     *mongo.Collection
	GlobalBookCollection       *mongo.Collection
	GlobalCategoryCollection   *mongo.Collection
	GlobalBookAuthorCollection *mongo.Collection
	GlobalBorrowBookCollection  *mongo.Collection
	GlobalReturnBookCollection *mongo.Collection
)

func ConnectDB() {
	// connect to db
	clientOptions := options.Client().ApplyURI("mongodb+srv://ydam:ydam@notes-cluster.iwril.mongodb.net/book?retryWrites=true&w=majority")
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatal(err)
	}
	if err := client.Ping(context.TODO(), nil); err != nil {
		log.Fatal(err)
	}
	fmt.Println("Connected to MongoDB!")
	GlobalClient = client

	// get collection
	GlobalAuthorCollection = client.Database("book").Collection("author")
	GlobalBookCollection = client.Database("book").Collection("book")
	GlobalCategoryCollection = client.Database("book").Collection("category")
	GlobalBookAuthorCollection = client.Database("book").Collection("book_author")
	GlobalBorrowBookCollection = client.Database("book").Collection("borrow_book")
	GlobalReturnBookCollection = client.Database("book").Collection("return_book")
}
